file { 'my_file':
	ensure  => present,
        mode    => '0644',
        path    => '/var/tmp/my_file.txt',
        replace => true,
        content => 'just testing!',
}
